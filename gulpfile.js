'use strict';

const fs = require('fs-extra'),
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    git = require('gulp-git'),
    browserify = require('browserify'),
    path = require('path');

const LIB_PATH = 'lib',
    LIB_GENERATED_PATH = path.join(LIB_PATH, 'generated'),
    LIB_GENERATED_UNITS_PATH = path.join(LIB_GENERATED_PATH, 'units.js'),
    ANGULAR_JS_PATH = 'angularJs',
    ANGULAR_JS_BROWSERIFY_TEMPLATES_PATH = path.join(ANGULAR_JS_PATH, 'browserify-templates'),
    ANGULAR_JS_GENERATED_PATH = path.join(ANGULAR_JS_PATH, 'generated'),
    ANGULAR_JS_GENERATED_UNITS_PATH = path.join(ANGULAR_JS_GENERATED_PATH, 'units.js'),
    ANGULAR_JS_DIST_PATH = path.join(ANGULAR_JS_PATH, 'dist'),
    ANGULAR_JS_DIST_FILE = 'sp-units',
    ANGULAR_JS_DIST_SRC = [path.join(ANGULAR_JS_PATH, 'app.js'), `./${ANGULAR_JS_PATH}/src/*`, `./${ANGULAR_JS_GENERATED_PATH}/*`];

module.exports = {
    default: gulp.series(generateUnits, browserifyAngularJs, angularJsDist),
    browserifyAngularJs,
    angularJsDist,
    generateUnits,
    watch
};

function angularJsDist() {
    return gulp.src(ANGULAR_JS_DIST_SRC)
        .pipe(concat(`${ANGULAR_JS_DIST_FILE}.js`))
        .pipe(gulp.dest(ANGULAR_JS_DIST_PATH))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename(`${ANGULAR_JS_DIST_FILE}.min.js`))
        .pipe(gulp.dest(ANGULAR_JS_DIST_PATH))
        .pipe(git.add());
}

async function browserifyAngularJs() {
    const names = await fs.readdir(ANGULAR_JS_BROWSERIFY_TEMPLATES_PATH);
    if (!names.length) {
        return;
    }

    await fs.ensureDir(ANGULAR_JS_GENERATED_PATH);

    for (const name of names) {
        const moduleName = _getModuleName(name);
        const [template, content] = await Promise.all([
            fs.readFile(path.join(ANGULAR_JS_BROWSERIFY_TEMPLATES_PATH, name), 'utf8'),
            new Promise((resolve, reject) => {
                browserify()
                    .require('./' + path.join(LIB_PATH, name), { expose: moduleName })
                    .transform('babelify', { presets: ['@babel/preset-env'] })
                    .bundle((err, result) => {
                        err ? reject(err) : resolve(result)
                    });
            })
        ]);

        await fs.writeFile(path.join(ANGULAR_JS_GENERATED_PATH, name), template.replace('{browserifyModuleName}', moduleName).replace('{browserifyContent}', content));
    }
}

function _getModuleName(name) {
    const splitName = name.replace(/ /g, '').split('.')[0].split('-'),
        res = [];
    for (const part of splitName) {
        if (!part) {
            return
        }

        res.push(part[0].toUpperCase() + part.slice(1));
    }
    return res.join('');
}

function watch() {
    gulp.watch('./units.json', gulp.series(generateUnits, browserifyAngularJs, angularJsDist));
    gulp.watch(`./${LIB_PATH}/*`, gulp.series(browserifyAngularJs, angularJsDist));
    gulp.watch(ANGULAR_JS_DIST_SRC, angularJsDist);
}

const UNITS_JS_FILE_ENUM_NAME = 'UNITS';

async function generateUnits() {
    const units = JSON.parse(await new Promise((resolve, reject) => {
        fs.readFile('units.json', (err, units) => err ? reject(err) : resolve(units))
    }));

    const unitsNamesCounter = 0,
        unitsByJsName = {},
        unitsByGroupsJs = [],
        unitsByNameJs = [],
        languageByNameJs = [],
        normalizersJs = [],
        allNamesSet = new Set();
    for (const [groupIndex, groupName] of Object.keys(units).entries()) {
        const groupTypesJs = [],
            normalizersTypesJs = [];
        for (const [typeIndex, type] of Object.keys(units[groupName]).entries()) {
            const typeUnitsJs = new Set();
            let normalizerUnitJs;
            for (const [unitIndex, unit] of units[groupName][type].units.entries()) {
                const unitJsName = 'unit' + groupIndex + typeIndex + unitIndex;
                unitsByJsName[unitJsName] = {
                    type: type,
                    size: unit.size,
                    names: unit.names,
                    accuracy: unit.accuracy,
                    normalizer: unit.normalizer,
                    universal: units[groupName][type].universal,
                    groupName
                };

                const existingUnitNames = new Set();
                for (const language of Object.keys(unit.names)) {
                    unit.names[language] = Array.isArray(unit.names[language]) ? unit.names[language] : [unit.names[language]];
                    for (const name of unit.names[language]) {
                        if (existingUnitNames.has(name)) {
                            continue;
                        }
                        if (allNamesSet.has(name)) {
                            throw new Error(`The unit name '${name}' is conflicting`);
                        }

                        existingUnitNames.add(name);
                        allNamesSet.add(name);
                        unitsByNameJs.push(JSON.stringify(name) + ': ' + UNITS_JS_FILE_ENUM_NAME + '.' + unitJsName);
                        languageByNameJs.push(JSON.stringify(name) + ': ' + JSON.stringify(language));
                    }
                    typeUnitsJs.add(UNITS_JS_FILE_ENUM_NAME + '.' + unitJsName);

                    if (unit.normalizer) {
                        normalizerUnitJs = UNITS_JS_FILE_ENUM_NAME + '.' + unitJsName;
                    }
                }
            }

            const typeJsonName = JSON.stringify(type);
            groupTypesJs.push(typeJsonName + ': { convert: ' + units[groupName][type].convert + ', units: [' + Array.from(typeUnitsJs).join(',') + ']}');
            normalizerUnitJs && normalizersTypesJs.push(typeJsonName + ': ' + normalizerUnitJs);
        }

        const groupJsonName = JSON.stringify(groupName);
        unitsByGroupsJs.push(groupJsonName + ': { ' + groupTypesJs.join(',') + ' }');
        normalizersTypesJs.length && normalizersJs.push(groupJsonName + ': { ' + normalizersTypesJs.join(',') + ' }');
    }

    const jsFileContent =
        '\'use strict\';\n\n' +
        'const ' + UNITS_JS_FILE_ENUM_NAME + ' = ' + JSON.stringify(unitsByJsName, null, '\t') + ';\n' +
        'module.exports = {\n' +
        '   GROUPS: { ' + unitsByGroupsJs.join(',') + ' }, \n' +
        '   BY_NAME: { ' + unitsByNameJs.join(',') + ' }, \n' +
        '   NORMALIZERS: { ' + normalizersJs.join(',') + ' },\n' +
        '   LANGUAGE_BY_NAME: { ' + languageByNameJs.join(',') + ' }\n' +
        '};';

    await new Promise((resolve, reject) => {
        fs.writeFile(LIB_GENERATED_UNITS_PATH, jsFileContent, err => err ? reject(err) : resolve());
    });

    await new Promise((resolve, reject) => {
        gulp.src(LIB_GENERATED_UNITS_PATH)
            .pipe(git.add())
            .on('finish', resolve)
            .on('error', reject);
    });
}