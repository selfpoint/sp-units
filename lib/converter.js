'use strict';

const DEFAULT_LANGUAGE = 'en',
    UNITS = require('./generated/units');

module.exports = {
    UNITS,

    convert
};

/**
 * Convert unit to group unit
 * @public
 *
 * @param {number} value
 * @param {string|Object} from
 * @param {Object} [options]
 * @param {string} [options.toGroup]
 * @param {string} [options.toUnit] - will override toGroup when the unit is found
 * @param {boolean} [options.convertInGroup=false]
 * @param {number} [options.minUnitSize=1]
 * @param {number} [options.defaultLanguage=1]
 *
 * @return { { names: Object, value: Number, accuracy: Number } }
 */
function convert(value, from, options) {
    options = options || {};

    const fromUnit = _unitByName(from);
    if (!fromUnit) {
        return {
            names: {
                [options.defaultLanguage || DEFAULT_LANGUAGE]: from
            },
            value: value,
            accuracy: 100
        };
    }

    let toUnit;
    // when has to unit, set the to group by it
    if (options.toUnit) {
        toUnit = _unitByName(options.toUnit);
        if (toUnit) {
            options.toGroup = toUnit.groupName;
        }
    }

    // when no toGroup, set it to the same as the from
    if (!options.toGroup) {
        options.toGroup = fromUnit.groupName;
    }

    const fromUnitRet = {
        names: { ...fromUnit.names },
        value: value,
        accuracy: fromUnit.accuracy
    };
    if (typeof from === 'string') {
        const language = UNITS.LANGUAGE_BY_NAME[from.toLowerCase()];
        fromUnitRet.names[language] = [
            from,
            ...fromUnit.names[language]
        ];
    }

    if ((!toUnit && !options.convertInGroup && fromUnit.groupName === options.toGroup) ||
        !UNITS.GROUPS[options.toGroup][fromUnit.type] || !UNITS.GROUPS[options.toGroup][fromUnit.type].convert) {
        return fromUnitRet;
    }

    toUnit = toUnit || _getUnitByGroupAndValue(UNITS.GROUPS[options.toGroup][fromUnit.type].units, value * fromUnit.size, options.minUnitSize);
    if (!toUnit) {
        return fromUnitRet;
    }

    if (toUnit.type !== fromUnit.type) {
        throw new Error(`Cannot parse from '${fromUnit.type}' to '${toUnit.type}' type`);
    }

    return {
        names: toUnit.names,
        value: value * fromUnit.size / toUnit.size,
        accuracy: toUnit.accuracy === undefined ? 100 : toUnit.accuracy
    };
}

/**
 * Get unit by name
 * @private
 *
 * @param {String|Object|Undefined} name
 *
 * @returns {Object|Undefined}
 */
function _unitByName(name) {
    if (!name || name && typeof name !== 'string') {
        return name;
    }

    return UNITS.BY_NAME[(name || '').toLowerCase()];
}

/**
 * Get unit by group value
 * @private
 *
 * @param {Object} group
 * @param {Number} value
 * @param {Number=1} [minUnitSize]
 *
 * @return {Object} unit
 */
function _getUnitByGroupAndValue(group, value, minUnitSize) {
    if (group.length === 1) {
        return group[0];
    }

    minUnitSize = minUnitSize || 1;

    for (var i = 1; i < group.length; i++) {
        if (value / group[i].size < minUnitSize) {
            return group[i - 1];
        }
    }

    return group[group.length - 1];
}