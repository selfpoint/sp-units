'use strict';

const UNITS = {
	"unit000": {
		"type": "mass",
		"size": 0.001,
		"names": {
			"en": [
				"mg",
				"milligram"
			],
			"he": [
				"מ\"ג",
				"מיליגרם"
			],
			"ru": [
				"мг.",
				"миллиграмм"
			],
			"es": [
				"mg",
				"miligramo"
			],
			"fr": [
				"mg",
				"milligramme"
			],
			"ar": [
				"مللي جرام"
			]
		},
		"accuracy": 10,
		"groupName": "eu"
	},
	"unit001": {
		"type": "mass",
		"size": 1,
		"names": {
			"en": [
				"gram",
				"gr",
				"g"
			],
			"he": [
				"גרם",
				"ג"
			],
			"ru": [
				"гр.",
				"грамм"
			],
			"es": [
				"g",
				"gramo"
			],
			"fr": [
				"gramme",
				"g",
				"gramme"
			],
			"ar": [
				"جرام"
			]
		},
		"accuracy": 10,
		"normalizer": 100,
		"groupName": "eu"
	},
	"unit002": {
		"type": "mass",
		"size": 1000,
		"names": {
			"en": [
				"kg",
				"kg",
				"kilogram"
			],
			"he": [
				"ק\"ג",
				"קילוגרם"
			],
			"ru": [
				"кг.",
				"килограмм"
			],
			"es": [
				"kg",
				"kilo",
				"kilogramo"
			],
			"fr": [
				"kilo",
				"kilo",
				"kilogramme"
			],
			"ar": [
				"كيلو جرام"
			]
		},
		"groupName": "eu"
	},
	"unit010": {
		"type": "volume",
		"size": 1,
		"names": {
			"en": [
				"ml",
				"milliliter"
			],
			"he": [
				"מ\"ל",
				"מיליליטר"
			],
			"ru": [
				"мл.",
				"миллилитр"
			],
			"es": [
				"ml",
				"mililitro"
			],
			"fr": [
				"ml",
				"millilitre"
			],
			"ar": [
				"مل"
			]
		},
		"accuracy": 10,
		"normalizer": 100,
		"groupName": "eu"
	},
	"unit011": {
		"type": "volume",
		"size": 1000,
		"names": {
			"en": [
				"liter",
				"ltr"
			],
			"he": [
				"ליטר",
				"ל"
			],
			"ru": [
				"литр",
				"л."
			],
			"es": [
				"litro",
				"l"
			],
			"fr": [
				"litre",
				"litre"
			],
			"ar": [
				"لتر"
			]
		},
		"groupName": "eu"
	},
	"unit020": {
		"type": "length",
		"size": 1,
		"names": {
			"en": [
				"cm",
				"centimeter"
			],
			"he": [
				"ס\"מ",
				"סנטימטר"
			],
			"ru": [
				"см.",
				"сантиметр"
			],
			"es": [
				"cm",
				"centímetro"
			],
			"fr": [
				"cm",
				"centimètre"
			],
			"ar": [
				"سم"
			]
		},
		"accuracy": 100,
		"groupName": "eu"
	},
	"unit021": {
		"type": "length",
		"size": 100,
		"names": {
			"en": [
				"m",
				"meter"
			],
			"he": [
				"מטר"
			],
			"ru": [
				"м.",
				"метр"
			],
			"es": [
				"m",
				"metro"
			],
			"fr": [
				"m",
				"mètre"
			],
			"ar": [
				"متر"
			]
		},
		"accuracy": 100,
		"normalizer": 100,
		"groupName": "eu"
	},
	"unit030": {
		"type": "area",
		"size": 1,
		"names": {
			"en": [
				"sq cm",
				"sq centimeter",
				"square centimeter"
			],
			"he": [
				"סמ\"ר",
				"סנטימטר רבוע",
				"סנטימטר מרובע"
			]
		},
		"accuracy": 100,
		"groupName": "eu"
	},
	"unit031": {
		"type": "area",
		"size": 10000,
		"names": {
			"en": [
				"sq m",
				"sq meter",
				"square meter"
			],
			"he": [
				"מ\"ר",
				"מטר רבוע",
				"מטר מרובע"
			]
		},
		"accuracy": 100,
		"groupName": "eu"
	},
	"unit100": {
		"type": "mass",
		"size": 28.349523125,
		"names": {
			"en": [
				"oz"
			],
			"he": [
				"אונקיה"
			],
			"ru": [
				"унция"
			],
			"es": [
				"oz",
				"onza"
			],
			"fr": [
				"oz",
				"once"
			],
			"ar": [
				"أونصة"
			]
		},
		"accuracy": 100,
		"groupName": "us"
	},
	"unit101": {
		"type": "mass",
		"size": 453.59237,
		"names": {
			"en": [
				"lb",
				"pound"
			],
			"he": [
				"פאונד"
			],
			"ru": [
				"фунт"
			],
			"es": [
				"lb",
				"libra"
			],
			"fr": [
				"lb",
				"livre"
			],
			"ar": [
				"رطل"
			]
		},
		"groupName": "us"
	},
	"unit110": {
		"type": "volume",
		"size": 29.5735,
		"names": {
			"en": [
				"fl oz",
				"floz"
			],
			"he": [
				"אונקיית נוזל"
			],
			"ru": [
				"жидкая унция"
			],
			"es": [
				"fl oz",
				"onza fluida"
			],
			"fr": [
				"fl oz",
				"once liquide"
			],
			"ar": [
				"أونصة سائلة"
			]
		},
		"accuracy": 100,
		"groupName": "us"
	},
	"unit111": {
		"type": "volume",
		"size": 3785.411784001,
		"names": {
			"en": [
				"gal",
				"gallon"
			],
			"he": [
				"גלון"
			],
			"ru": [
				"галлон"
			],
			"es": [
				"gal",
				"galón"
			],
			"fr": [
				"gallon"
			],
			"ar": [
				"جالون"
			]
		},
		"groupName": "us"
	},
	"unit120": {
		"type": "length",
		"size": 2.54,
		"names": {
			"en": [
				"in",
				"inch"
			],
			"he": [
				"אינטש"
			],
			"ru": [
				"дюйм"
			],
			"fr": [
				"pouce"
			],
			"ar": [
				"بوصة"
			]
		},
		"accuracy": 100,
		"groupName": "us"
	},
	"unit121": {
		"type": "length",
		"size": 30.48,
		"names": {
			"en": [
				"ft",
				"feet"
			],
			"he": [
				"רגל"
			],
			"ru": [
				"фут"
			],
			"fr": [
				"pieds"
			],
			"ar": [
				"قدم"
			]
		},
		"accuracy": 100,
		"groupName": "us"
	},
	"unit130": {
		"type": "area",
		"size": 6.4516,
		"names": {
			"en": [
				"sq in",
				"sq inch",
				"square inch"
			],
			"he": [
				"אינטש רבוע"
			],
			"fr": [
				"pouce carré"
			],
			"ar": [
				"بوصة مربعة"
			]
		},
		"accuracy": 100,
		"groupName": "us"
	},
	"unit131": {
		"type": "area",
		"size": 929.03,
		"names": {
			"en": [
				"sq ft",
				"sq feet",
				"square feet"
			],
			"he": [
				"רגל רבוע"
			],
			"fr": [
				"pieds carrés"
			],
			"ar": [
				"قدم مربع"
			]
		},
		"accuracy": 100,
		"groupName": "us"
	},
	"unit200": {
		"type": "unit",
		"size": 1,
		"names": {
			"en": [
				"unit",
				"units",
				"item",
				"items",
				"ct",
				"count"
			],
			"he": [
				"יח'",
				"יח",
				"יחידה",
				"יחידות"
			],
			"ru": [
				"Ед. изм"
			],
			"es": [
				"unidad"
			],
			"fr": [
				"unité",
				"unités"
			],
			"ar": [
				"عنصر"
			]
		},
		"normalizer": 10,
		"universal": true,
		"groupName": "universal"
	}
};
module.exports = {
   GROUPS: { "eu": { "mass": { convert: true, units: [UNITS.unit000,UNITS.unit001,UNITS.unit002]},"volume": { convert: true, units: [UNITS.unit010,UNITS.unit011]},"length": { convert: true, units: [UNITS.unit020,UNITS.unit021]},"area": { convert: true, units: [UNITS.unit030,UNITS.unit031]} },"us": { "mass": { convert: true, units: [UNITS.unit100,UNITS.unit101]},"volume": { convert: false, units: [UNITS.unit110,UNITS.unit111]},"length": { convert: true, units: [UNITS.unit120,UNITS.unit121]},"area": { convert: true, units: [UNITS.unit130,UNITS.unit131]} },"universal": { "unit": { convert: false, units: [UNITS.unit200]} } }, 
   BY_NAME: { "mg": UNITS.unit000,"milligram": UNITS.unit000,"מ\"ג": UNITS.unit000,"מיליגרם": UNITS.unit000,"мг.": UNITS.unit000,"миллиграмм": UNITS.unit000,"miligramo": UNITS.unit000,"milligramme": UNITS.unit000,"مللي جرام": UNITS.unit000,"gram": UNITS.unit001,"gr": UNITS.unit001,"g": UNITS.unit001,"גרם": UNITS.unit001,"ג": UNITS.unit001,"гр.": UNITS.unit001,"грамм": UNITS.unit001,"gramo": UNITS.unit001,"gramme": UNITS.unit001,"جرام": UNITS.unit001,"kg": UNITS.unit002,"kilogram": UNITS.unit002,"ק\"ג": UNITS.unit002,"קילוגרם": UNITS.unit002,"кг.": UNITS.unit002,"килограмм": UNITS.unit002,"kilo": UNITS.unit002,"kilogramo": UNITS.unit002,"kilogramme": UNITS.unit002,"كيلو جرام": UNITS.unit002,"ml": UNITS.unit010,"milliliter": UNITS.unit010,"מ\"ל": UNITS.unit010,"מיליליטר": UNITS.unit010,"мл.": UNITS.unit010,"миллилитр": UNITS.unit010,"mililitro": UNITS.unit010,"millilitre": UNITS.unit010,"مل": UNITS.unit010,"liter": UNITS.unit011,"ltr": UNITS.unit011,"ליטר": UNITS.unit011,"ל": UNITS.unit011,"литр": UNITS.unit011,"л.": UNITS.unit011,"litro": UNITS.unit011,"l": UNITS.unit011,"litre": UNITS.unit011,"لتر": UNITS.unit011,"cm": UNITS.unit020,"centimeter": UNITS.unit020,"ס\"מ": UNITS.unit020,"סנטימטר": UNITS.unit020,"см.": UNITS.unit020,"сантиметр": UNITS.unit020,"centímetro": UNITS.unit020,"centimètre": UNITS.unit020,"سم": UNITS.unit020,"m": UNITS.unit021,"meter": UNITS.unit021,"מטר": UNITS.unit021,"м.": UNITS.unit021,"метр": UNITS.unit021,"metro": UNITS.unit021,"mètre": UNITS.unit021,"متر": UNITS.unit021,"sq cm": UNITS.unit030,"sq centimeter": UNITS.unit030,"square centimeter": UNITS.unit030,"סמ\"ר": UNITS.unit030,"סנטימטר רבוע": UNITS.unit030,"סנטימטר מרובע": UNITS.unit030,"sq m": UNITS.unit031,"sq meter": UNITS.unit031,"square meter": UNITS.unit031,"מ\"ר": UNITS.unit031,"מטר רבוע": UNITS.unit031,"מטר מרובע": UNITS.unit031,"oz": UNITS.unit100,"אונקיה": UNITS.unit100,"унция": UNITS.unit100,"onza": UNITS.unit100,"once": UNITS.unit100,"أونصة": UNITS.unit100,"lb": UNITS.unit101,"pound": UNITS.unit101,"פאונד": UNITS.unit101,"фунт": UNITS.unit101,"libra": UNITS.unit101,"livre": UNITS.unit101,"رطل": UNITS.unit101,"fl oz": UNITS.unit110,"floz": UNITS.unit110,"אונקיית נוזל": UNITS.unit110,"жидкая унция": UNITS.unit110,"onza fluida": UNITS.unit110,"once liquide": UNITS.unit110,"أونصة سائلة": UNITS.unit110,"gal": UNITS.unit111,"gallon": UNITS.unit111,"גלון": UNITS.unit111,"галлон": UNITS.unit111,"galón": UNITS.unit111,"جالون": UNITS.unit111,"in": UNITS.unit120,"inch": UNITS.unit120,"אינטש": UNITS.unit120,"дюйм": UNITS.unit120,"pouce": UNITS.unit120,"بوصة": UNITS.unit120,"ft": UNITS.unit121,"feet": UNITS.unit121,"רגל": UNITS.unit121,"фут": UNITS.unit121,"pieds": UNITS.unit121,"قدم": UNITS.unit121,"sq in": UNITS.unit130,"sq inch": UNITS.unit130,"square inch": UNITS.unit130,"אינטש רבוע": UNITS.unit130,"pouce carré": UNITS.unit130,"بوصة مربعة": UNITS.unit130,"sq ft": UNITS.unit131,"sq feet": UNITS.unit131,"square feet": UNITS.unit131,"רגל רבוע": UNITS.unit131,"pieds carrés": UNITS.unit131,"قدم مربع": UNITS.unit131,"unit": UNITS.unit200,"units": UNITS.unit200,"item": UNITS.unit200,"items": UNITS.unit200,"ct": UNITS.unit200,"count": UNITS.unit200,"יח'": UNITS.unit200,"יח": UNITS.unit200,"יחידה": UNITS.unit200,"יחידות": UNITS.unit200,"Ед. изм": UNITS.unit200,"unidad": UNITS.unit200,"unité": UNITS.unit200,"unités": UNITS.unit200,"عنصر": UNITS.unit200 }, 
   NORMALIZERS: { "eu": { "mass": UNITS.unit001,"volume": UNITS.unit010,"length": UNITS.unit021 },"universal": { "unit": UNITS.unit200 } },
   LANGUAGE_BY_NAME: { "mg": "en","milligram": "en","מ\"ג": "he","מיליגרם": "he","мг.": "ru","миллиграмм": "ru","miligramo": "es","milligramme": "fr","مللي جرام": "ar","gram": "en","gr": "en","g": "en","גרם": "he","ג": "he","гр.": "ru","грамм": "ru","gramo": "es","gramme": "fr","جرام": "ar","kg": "en","kilogram": "en","ק\"ג": "he","קילוגרם": "he","кг.": "ru","килограмм": "ru","kilo": "es","kilogramo": "es","kilogramme": "fr","كيلو جرام": "ar","ml": "en","milliliter": "en","מ\"ל": "he","מיליליטר": "he","мл.": "ru","миллилитр": "ru","mililitro": "es","millilitre": "fr","مل": "ar","liter": "en","ltr": "en","ליטר": "he","ל": "he","литр": "ru","л.": "ru","litro": "es","l": "es","litre": "fr","لتر": "ar","cm": "en","centimeter": "en","ס\"מ": "he","סנטימטר": "he","см.": "ru","сантиметр": "ru","centímetro": "es","centimètre": "fr","سم": "ar","m": "en","meter": "en","מטר": "he","м.": "ru","метр": "ru","metro": "es","mètre": "fr","متر": "ar","sq cm": "en","sq centimeter": "en","square centimeter": "en","סמ\"ר": "he","סנטימטר רבוע": "he","סנטימטר מרובע": "he","sq m": "en","sq meter": "en","square meter": "en","מ\"ר": "he","מטר רבוע": "he","מטר מרובע": "he","oz": "en","אונקיה": "he","унция": "ru","onza": "es","once": "fr","أونصة": "ar","lb": "en","pound": "en","פאונד": "he","фунт": "ru","libra": "es","livre": "fr","رطل": "ar","fl oz": "en","floz": "en","אונקיית נוזל": "he","жидкая унция": "ru","onza fluida": "es","once liquide": "fr","أونصة سائلة": "ar","gal": "en","gallon": "en","גלון": "he","галлон": "ru","galón": "es","جالون": "ar","in": "en","inch": "en","אינטש": "he","дюйм": "ru","pouce": "fr","بوصة": "ar","ft": "en","feet": "en","רגל": "he","фут": "ru","pieds": "fr","قدم": "ar","sq in": "en","sq inch": "en","square inch": "en","אינטש רבוע": "he","pouce carré": "fr","بوصة مربعة": "ar","sq ft": "en","sq feet": "en","square feet": "en","רגל רבוע": "he","pieds carrés": "fr","قدم مربع": "ar","unit": "en","units": "en","item": "en","items": "en","ct": "en","count": "en","יח'": "he","יח": "he","יחידה": "he","יחידות": "he","Ед. изм": "ru","unidad": "es","unité": "fr","unités": "fr","عنصر": "ar" }
};