# sp-units
convert units (kg, g, lb and more)

## units.json
#### groups
all the units size related as below:

* mass -> gram
* volume -> ml
* length -> cm
* area -> square cm

#### names
each `unit.names` must have at least one name in english (en)
 
#### languages
* English - en - 0
* Hebrew  - he - 1
* Russian - ru - 2

## commit
before any commit do `gulp` (to create dist file and build units)
# test
