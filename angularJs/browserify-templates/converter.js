(function (angular) {
    'use strict';

    var require;
    {browserifyContent};

    var coreConverter = require('{browserifyModuleName}');

    angular.module('spUnits')
        .constant('SP_UNITS', coreConverter.UNITS)
        .service('spUnitsCoreConverter', [function() {
            var self = this;

            self.convert = coreConverter.convert;
        }]);
})(angular);
