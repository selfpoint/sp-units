(function (angular) {
    'use strict';

    function getName(names) {
        return Array.isArray(names) ? names[0] : names;
    }

    angular.module('spUnits')
        .filter('spUnits', ['spUnits', 'spUnitsConfig', function (spUnits, spUnitsConfig) {
            /**
             * @param {Number} weight
             * @param {String} from
             * @param {Object} [options]
             * @param {String} [options.to]
             * @param {String} [options.language]
             * @param {Boolean} [options.convertInGroup]
             * @param {String} [options.separator]
             *
             * @returns {String}
             */
            function spUnitsFilter(weight, from, options) {
                if (!weight) {
                    return '';
                } else if (!from) {
                    return _weightAccuracy(weight);
                }

                options = options || {};

                var convert = spUnits.convert(weight, from, options);
                return _weightAccuracy(convert.value, convert.accuracy) + (options.separator || ' ') + getName(convert.names[options.language] || convert.names[spUnitsConfig.getDefaultLanguage()] || convert.names.en);
            }

            /**
             * Return weight string with accuracy
             * @private
             *
             * @param {Number} weight
             * @param {Number=100} [accuracy]
             *
             * @returns {String}
             */
            function _weightAccuracy(weight, accuracy) {
                accuracy = accuracy || 100;

                var totalWeight = (weight * accuracy) / accuracy;
                var totalWeightSplit = totalWeight.toString().split('.');

                if(totalWeightSplit.length === 1 || totalWeightSplit[1].length <= 2) {
                    return totalWeight.toString();
                }

                return totalWeight.toFixed(2);
            }

            spUnitsFilter.$stateful = true;
            return spUnitsFilter;
        }])
		.filter('spUnitsPrice', ['spUnits', 'spUnitsConfig', '$filter', function(spUnits, spUnitsConfig, $filter) {
			function spUnitsPriceFilter(price, weight, unitName, language, quantityNormalizer, lengthNormalizer) {
				if (!price || !weight || !unitName) return '';

                var convert = spUnits.normalizePrice(price, weight, unitName, Number(quantityNormalizer)),
                    isSingle = convert && convert.normalizer === 1,
                    isFrUnit = (language === 'fr' &&  unitName === 'unité'),
                    fixedUnitPrice = '',
                    per = spUnitsConfig.translate('PER'),
                    divider = ' ';

                if (!convert) {
                    return '';
                }

                // Override length normalizer to 1 meter according to retailer settings
                if (lengthNormalizer === "1" && convert.normalizer === 100 && convert.names.en.includes('meter')) {
                    convert.normalizer = 1;
                    convert.price = convert.price/100;
                }

                if(isSingle) {
                    convert.normalizer = '';

                    if(language === 'he') {
                        per = 'ל';
                        divider = '';
                    }

                    if(isFrUnit) {
                        per = 'l\''
                        divider = '';
                    }
                }

                fixedUnitPrice = (convert.price ? $filter('currency')(convert.price) : '') + ' ' + per +
                    convert.normalizer + divider + getName(convert.names[language] || convert.names[spUnitsConfig.getDefaultLanguage()] || convert.names.en);

                if(isFrUnit && !isSingle) {
                    fixedUnitPrice = (convert.price ? $filter('currency')(convert.price) : '') + ' ' +
                        convert.normalizer + ' l\'' + getName(convert.names[language] || convert.names[spUnitsConfig.getDefaultLanguage()] || convert.names.en);
                }

                return fixedUnitPrice;
			}

			spUnitsPriceFilter.$stateful = true;
			return spUnitsPriceFilter;
		}])
        .filter('spDynamicUnitsPrice', ['spUnits', 'spUnitsConfig', '$filter', function(spUnits, spUnitsConfig, $filter) {
            function spDynamicUnitsPriceFilter(priceNormalization, weight) {
                if (!priceNormalization || !weight) return '';

                var per = spUnitsConfig.translate('PER');

                return  $filter('currency')(priceNormalization) + ' ' + per + weight;
            }

            spDynamicUnitsPriceFilter.$stateful = true;
            return spDynamicUnitsPriceFilter;
        }]);
})(angular);
