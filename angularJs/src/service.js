(function (angular) {
    'use strict';

    angular.module('spUnits')
        .service('spUnits', ['SP_UNITS', 'spUnitsConfig', 'spUnitsCoreConverter', function (SP_UNITS, spUnitsConfig, spUnitsCoreConverter) {
            var self = this;

            self.convert = convert;
            self.normalizePrice = normalizePrice;

            /**
             * Convert unit to group unit
             * @public
             *
             * @param {number} value
             * @param {string|Object} from
             * @param {Object} [options]
             * @param {string|Object} [options.to]
             * @param {Boolean=false} [options.convertInGroup]
             * @param {number} [options.minUnitSize=1]
             *
             * @return { { names: Object, value: Number, accuracy: Number } }
             */
            function convert (value, from, options) {
                options = options || {};

                return spUnitsCoreConverter.convert(value, from, {
                    toUnit: options.to,
                    toGroup: (spUnitsConfig.getGroup() || {}).name,
                    convertInGroup: options.convertInGroup,
                    minUnitSize: options.minUnitSize,
                    defaultLanguage: spUnitsConfig.getDefaultLanguage()
                });
            }

            /**
             * Normalize the price to standard unit
             * @public
             *
             * @param {Number} price
             * @param {Number} weight
             * @param {String} unitName
             * @param {number} normalizer
             * 
             * @return { Null | { names: Object, normalizer: Number, price: Number } }
             */
            function normalizePrice(price, weight, unitName, normalizer) {
                var unit = SP_UNITS.BY_NAME[unitName.toLowerCase()];
                if (!unit) {
                    return null;
                }

                var group = unit.universal ? { name: unit.groupName } : spUnitsConfig.getGroup();
                if (!group || !SP_UNITS.NORMALIZERS[group.name] || !SP_UNITS.NORMALIZERS[group.name][unit.type]) {
                    return null;
                }

                var viewNormalyzerConversion,
                    normalizerUnit = SP_UNITS.NORMALIZERS[group.name][unit.type],
                    normalizerValue = spUnitsConfig.getNormalizerOverride(unit.type) || normalizerUnit.normalizer,
                    normalizerWeightUnit = self.convert(weight, unit, { to: normalizerUnit })

                if (normalizerUnit.universal && normalizerWeightUnit.value <= normalizerValue) {
                    return null;
                }

                if(normalizerUnit.type !== 'unit'){
                    normalizer = null;
                }

                if (normalizerValue !== normalizerUnit.normalizer) {
                    viewNormalyzerConversion = self.convert(normalizerValue, normalizerUnit, {convertInGroup: true});
                }

                var names = angular.extend({}, (viewNormalyzerConversion || normalizerUnit).names),
                  viewNormalizerUnit = SP_UNITS.BY_NAME[Object.values(names)[0][0].toLowerCase()];
                if (viewNormalizerUnit === unit) {
                    var language = SP_UNITS.LANGUAGE_BY_NAME[unitName.toLowerCase()];
                    names[language] = [unitName].concat(names[language]);
                }

                return {
                    names: names,
                    normalizer: normalizer || (viewNormalyzerConversion && viewNormalyzerConversion.value) || normalizerValue,
                    price: price / (normalizerWeightUnit.value / (normalizer || normalizerValue))
                };
            }
        }]);
})(angular);
