(function (angular) {
    'use strict';

    angular.module('spUnits')
        .constant('SP_UNITS_TRANSLATE', {
            en: {
                PER: 'per '
            },
            he: {
                PER: 'ל-'
            },
            ru: {
                PER: 'за '
            },
            es: {
                PER: 'por '
            },
			fr: {
				PER: 'le '
			},
			ar: {
				PER: 'لكل '
			}
        })
        .service('spUnitsConfig', ['SP_UNITS', 'SP_UNITS_TRANSLATE', function (SP_UNITS, SP_UNITS_TRANSLATE) {
            var self = this;

            /**
             * @type {String}
             * @private
             */
            self._defaultCurrency = '';

            /**
             * Set default currency
             * @public
             *
             * @param {String} currency
             */
            self.setCurrency = function (currency) {
                self._defaultCurrency = currency;
            };

            /**
             * Get currency
             * @public
             *
             * @return {String} currency
             */
            self.getCurrency = function () {
                return self._defaultCurrency;
            };

            /**
             * @type {Object}
             * @private
             */
            self._group;

            /**
             * Set units group
             * @public
             *
             * @param {String} group
             */
            self.setGroup = function (group) {
                self._group = {
                    name: group,
                    units: SP_UNITS.GROUPS[group]
                };
            };

            /**
             * Set units group
             * @public
             *
             * @param {Object} massUnitNames
             * @param {string} fallbackGroup
             */
            self.setGroupByMassUnit = function (massUnitNames, fallbackGroup) {
                var group;
                angular.forEach(massUnitNames, function(name, languageId) {
                    if (!group && SP_UNITS.BY_NAME[name.toLowerCase()]) {
                        group = SP_UNITS.BY_NAME[name.toLowerCase()].groupName;
                    }
                });

                self.setGroup(group || fallbackGroup);
            };

            /**
             * Get units group
             * @public
             */
            self.getGroup = function () {
                return self._group;
            };

            /**
             * @type {String}
             * @private
             */
            self._defaultLanguage = 'en';

            /**
             * Set default language of unit names
             * @public
             *
             * @param {String} language
             */
            self.setDefaultLanguage = function (language) {
                self._defaultLanguage = language;
            };

            /**
             * Get default language of unit names
             * @public
             *
             * @return {String} language
             */
            self.getDefaultLanguage = function () {
                return self._defaultLanguage;
            };

            /**
             * Translate to _defaultLanguage or to en
             * @public
             *
             * @param {String} key
             *
             * @return {String}
             */
            self.translate = function (key) {
                var def = SP_UNITS_TRANSLATE[self._defaultLanguage];
                return def && def[key] ? def[key] : SP_UNITS_TRANSLATE.en[key];
            };

            /**
             * @type {Object}
             * @private
             */
            self._normalizerOverrides = {};

            /**
             * Set normalizer overrides for different unit types
             * @public
             * 
             * @param {Object} normalizerOverrides
             */
            self.setNormalizerOverrides = function(normalizerOverrides) {
              angular.forEach(normalizerOverrides, function (normlalizerValue, unitType) {
                self._normalizerOverrides[unitType] = normlalizerValue;
              });
            };

            /**
             * Get normalizer override for specific unit type
             * @public
             * 
             * @param {string} unitType
             *
             * @return {number|undefined}
             */
            self.getNormalizerOverride = function(unitType) {
              return self._normalizerOverrides[unitType];
            };
        }]);
})(angular);
